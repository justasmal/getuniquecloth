<?php


namespace GetUniqueCloth;


use Exception;
use Predis\Client;

class RedisControl
{

    private static $redis;

    /**
     * @return Client
     */
    public static function common()
    {
        if (empty(self::$redis)) {
            try {
                self::$redis = new Client(array(
                        "scheme" => "tcp",
                        "host" => getenv("REDIS_URL"),
                        "port" => getenv("REDIS_PORT"),
                        //"password" => getenv("REDIS_PASS"),
                        "persistent" => "1")
                );
            } catch (Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
        return self::$redis;
    }

    public static function getPong()
    {
        try {
            self::common()->ping();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public static function addUnique($cloth) {
        RedisControl::common()->sadd("cloths", $cloth);
    }
    public static function GetSet($set) {
        return RedisControl::common()->smembers("cloths");
    }
}
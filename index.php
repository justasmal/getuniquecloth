<?php

require __DIR__ . '/vendor/autoload.php';

use GetUniqueCloth\RedisControl;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;



for ($i = 0; $i < 60; $i++) {
    if (RedisControl::getPong() == true) {
        var_dump("set");
        break;
    } else {
        sleep(1);
    }
}


$client = new Client();

$requests = function () {
    $offset = 0;

    for ($i = 0; $i < 1100; $i++) {
        $uri = 'https://www.zalando.co.uk/api/catalog/articles?categories=womens-clothing&limit=84&offset=' . $offset .'&sort=activation_date';
        $offset += 84;
        var_dump($uri);
        yield new Request('GET', $uri);
    }
};



$pool = new Pool($client, $requests(), [
    'concurrency' => 5,
    'fulfilled' => function (Response $response, $index) {
        $body = \GuzzleHttp\json_decode($response->getBody());
        foreach ($body->articles as $article) {
            RedisControl::addUnique($article->sku);
        }
    },
    'rejected' => function (RequestException $reason, $index) {
        echo "error\n";
    },
]);


$promise = $pool->promise();

$promise->wait();

echo "\n\nOver\n";

var_dump(count(RedisControl::GetSet("cloths")));
